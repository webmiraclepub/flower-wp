try{
	document.addEventListener('DOMContentLoaded', ()=>{
		let construct = new MyConstructor();
	});
}
catch( e ){
	console.error('error');
}

class MyConstructor{
	constructor( ){
		let input = document.querySelectorAll('#canvas_element input');
		for (let i = 0; i < input.length; i++) {
			input[i].addEventListener( 'click', (e)=>{
				let value = e.target.value;
				let color = '#101010';
				if (value == 'light')
					color = '#ffffff';
				else if (value == 'black')
					color = '#000000';
				let obj = e.target.parentNode.parentNode.dataset.obj;
				if (obj == 'st16')
					obj = 'st50';
				else if (obj == 'st5')
					obj = 'st1';
				else if (obj == 'st13')
					obj = 'st3';

				this.setCanvasColor(obj, color);
			});
		}

		let li = document.querySelectorAll( '#canvas_navbar ul li' );
		for (let i = 0; i < li.length; i++) {
			let color = li[i].dataset.color;
			li[i].style.backgroundColor = color;
			li[i].addEventListener( 'click', (e)=>{
				let obj = document.querySelector('.construction__right-settings-element_active').dataset.obj;
				let color = e.target.dataset.color;
				let active = e.target.dataset.active;
				document.querySelector( '#canvas_navbar ul li.is-active' ).classList.remove( 'is-active' );
				e.target.classList.add( 'is-active' );
				this.setCanvasColor( obj, color );
				document.querySelector('.construction__right-settings-element_active').dataset.active = active;

				this.setSaveUrl();
			});
		}

		let elements = document.querySelectorAll('.construction__right-settings-element');
		for (let i = 0; i < elements.length; i++) {
			elements[i].addEventListener( 'click', (e)=>{
				let path = e.path;
				let success = false;
				let element = '';
				for (let i = 0; i < path.length; i++) {
					if ( path[i].classList.contains( 'construction__right-settings-element' ) ){
						element = path[i];
						success = true;
						break;
					}
				}
				if ( success ){
					let active_color = e.target.dataset.active;
					document.querySelector( '.construction__right-settings-element_active' ).classList.remove('construction__right-settings-element_active');
					element.classList.add( 'construction__right-settings-element_active' );
				}
				this.setSaveUrl();
			});
			let obj = elements[i].dataset.obj;
			let active_color = elements[i].dataset.active;
			let color = document.querySelector('#canvas_navbar ul li:nth-of-type('+active_color+')').dataset.color;
			this.setCanvasColor( obj, color );
		}

		let sets = document.querySelectorAll( '#canvas_sets ul li' );
		for (let i = 0; i < sets.length; i++) {
			sets[i].addEventListener( 'click', (e)=>{
				let ul = e.target.parentNode;
				let lis = ul.querySelectorAll( 'li' );
				let obj = '';
				let color = '';
				for (let i = 0; i < lis.length; i++) {
					obj = lis[i].dataset.obj;
					color = lis[i].dataset.color;
					this.setCanvasColor( obj, color );
				}
				this.setSaveUrl();
			});
			let color = sets[i].dataset.color;
			sets[i].style.backgroundColor = color;
		}
		this.setSaveUrl();

		this.listenOrderButton();
	}

	listenOrderButton(){
		// let self = this;
		// let form = document.querySelector('.modalwindow form');
		// let sumbit_but = form.querySelector('#submit_but');
		// sumbit_but.addEventListener( 'click', (e)=> {
		// 	e.preventDefault();
		// 	let xhr = new XMLHttpRequest();
		// 	let body = new Map();
		// 	body.set( 'action', 'send_message' );
		// 	body.set( 'big_flower_list', this.big_flower_list );
		// 	body.set( 'middle_flower_list', this.middle_flower_list );
		// 	body.set( 'small_flower_list', this.small_flower_list );
		// 	body.set( 'lisitiki', this.lisitiki );
		// 	body.set( 'stena', this.stena );
		// 	body.set( 'tuchinki', this.tuchinki );
		// 	body.set( 'nonce', ajax.nonce );
		// 	body.set( 'firstname', form.querySelector('input[name="firstname"]').value );
		// 	body.set( 'phone', form.querySelector('input[name="phone"]').value );
		// 	body.set( 'email', form.querySelector('input[name="email"]').value );
		// 	body.set( 'subject', form.querySelector('textarea[name="subject"]').value );
		// 	body.set( 'checkbox', form.querySelector('input[name="checkbox"]').checked );

		// 	let xhr_body = '';
		// 	for( let val of body ){
		// 		xhr_body += encodeURIComponent( val[0] ) + '=' + encodeURIComponent( val[1] ) + '&';
		// 	}

		// 	xhr.open("POST", ajax.url, true);
		// 	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');

		// 	xhr.send(xhr_body);
		// 	xhr.onreadystatechange = function(){
		// 		console.log( arguments );
		// 		//if (xhr.readyState == 4 && xhr.status == 200) location.href = '<?= get_field( 'miracle-global-form-redirect', 'option' ) ?>';
		// 	};

		// 	xhr.onload = function( data ){
		// 		console.log( data );
		// 	}
		// });
	}

	setSaveUrl(){
		let svg = document.querySelector('#Layer_1');

		this.big_flower_list = svg.querySelector('.st16').style.fill;
		this.middle_flower_list = svg.querySelector('.st5').style.fill;
		this.small_flower_list = svg.querySelector('.st13').style.fill;
		this.lisitiki = svg.querySelector('.st51').style.fill;
		this.stena = svg.querySelector('.st0').style.fill;
		this.tuchinki = svg.querySelector('.st50').style.fill;

		let rect = svg.getBoundingClientRect();
		let h = rect.height;
		let w = rect.width;
		let html = svg.parentNode.innerHTML;
		let imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
		let canvas = document.createElement("canvas");
		document.body.appendChild( canvas );
		let context = canvas.getContext("2d");
		canvas.width = w;
		canvas.height = h;

		let image = new Image;
		image.src = imgsrc;
		image.onload = function() {
			// console.log(image);
			context.drawImage(image, 0, 0);      
			// canvas = encodeURIComponent(canvas);
			// let canvasdata = canvas.toDataURL("image/png");
			// console.log(canvasdata);
			save.setAttribute( 'download', 'export_'+Date.now()+'.svg' );
			// save.setAttribute( 'href', canvasdata );
			save.setAttribute( 'href', image.currentSrc );
			canvas.parentNode.removeChild(canvas);
		};
	}

	setCanvasColor( obj, color ){
		let elements = document.querySelectorAll( '.' + obj );
		for (let i = 0; i < elements.length; i++) {
			elements[i].style.fill = color;
		}
	}
}
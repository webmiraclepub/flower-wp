/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _miracleslider = __webpack_require__(1);

var _perfectPosition = __webpack_require__(2);

try {
	document.addEventListener('DOMContentLoaded', function () {

		// main slider
		var container = document.querySelector('.miracle-slider-main');
		var args = {
			itemCountPerSlide: {
				320: 1,
				780: 1,
				1200: 1
			},
			autoPlay: true,
			container: container,
			margin: 0
		};
		var slider = new _miracleslider.MiracleSlider(args);
		// main slider

		// aboutus_slider
		container = document.querySelector('.aboutus__slider');
		args = {
			itemCountPerSlide: {
				320: 2,
				780: 3,
				1200: 3
			},
			autoPlay: true,
			container: container,
			margin: 10
		};
		var about_slider = new _miracleslider.MiracleSlider(args);
		// aboutus_slider

		// portfolio slider
		container = document.querySelector('.portfolio__slider');
		args = {
			itemCountPerSlide: {
				320: 2,
				780: 3,
				1200: 4
			},
			autoPlay: true,
			container: container,
			margin: 20,
			rows: 2
		};
		var portfolio__slider = new _miracleslider.MiracleSlider(args);
		// portfolio slider

		// team slider
		container = document.querySelector('.team__slider');
		args = {
			itemCountPerSlide: {
				320: 1,
				780: 3,
				1200: 3
			},
			container: container,
			margin: 15
		};
		var team_slider = new _miracleslider.MiracleSlider(args);
		// team slider

		// correct position
		var correct_blocks = [];
		var block = {
			blockName: document.querySelector('.test-header__laye'),
			offset: 0
		};
		correct_blocks.push(block);

		var perfect = new _perfectPosition.PerfectPosition(correct_blocks);
		// 


		var toggle = document.querySelector('.test-header__mobile-menu-toggle');
		// console.log(toggle);
		toggle.addEventListener('click', function (e) {
			document.querySelector('.test-header__menu').classList.toggle('test-header__menu_toggled');
		});

		var modal = document.querySelector('.modalwindow');
		var btn = document.querySelector("#test-header__myBtn");
		var span = document.querySelector(".modalwindow__close");
		var btn2 = document.querySelector('#zayavka');

		btn.onclick = function (e) {
			e.preventDefault();
			modal.classList.add('modalwindow_opened');
		};

		btn2.onclick = function (e) {
			e.preventDefault();
			modal.classList.add('modalwindow_opened');
		};

		span.onclick = function () {
			modal.classList.add('modalwindow_opened');
		};

		window.onclick = function (event) {
			if (event.target == span) {
				modal.classList.remove('modalwindow_opened');
			}
		};
	});
} catch (e) {
	console.error(e);
}

// try{
// 	document.addEventListener('DOMContentLoaded', function(){
// 		let container = document.querySelector('.miracle-slider-about');
// 		let args = {
// 			itemCountPerSlide : 3,
// 			autoPlay : true,
// 			container : container
// 		};
// 		let x = new MiracleSlider( args );
// 	});
// }

// catch(e){
// 	console.error(e);
// }

// import {scroll} from './lib/scroll';

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MiracleSlider = function () {
	function MiracleSlider(args) {
		_classCallCheck(this, MiracleSlider);

		this.defaultArgs(args);
		if (this.createWrap(args)) {
			this.setBehavior(args);
			this.listenNav(args);
		}
	}

	_createClass(MiracleSlider, [{
		key: 'defaultArgs',
		value: function defaultArgs(args) {
			try {
				if (args.itemCountPerSlide == undefined) args.itemCountPerSlide = 3;
				if (screen.width <= 320) args.itemCountPerSlide = args.itemCountPerSlide[320];else if (screen.width <= 780) args.itemCountPerSlide = args.itemCountPerSlide[780];else if (screen.width <= 1200) args.itemCountPerSlide = args.itemCountPerSlide[1200];else args.itemCountPerSlide = args.itemCountPerSlide[1200];

				if (args.autoPlay == undefined) args.autoPlay = false;
				if (args.container == undefined) console.info('no container chosen');
				if (args.margin == undefined) args.margin = 10;
				if (args.rows == undefined) args.rows = 1;
			} catch (e) {
				console.log('Error in plugin');
				console.info(e);
			}
		}
	}, {
		key: 'createWrap',
		value: function createWrap(args) {
			try {
				var items = args.container.querySelectorAll('.miracle-slider__item');

				var outer = document.createElement('div');
				outer.classList.add('miracle-slider__outer');

				var stage = document.createElement('div');
				stage.classList.add('miracle-slider__stage');

				for (var i = 0; i < items.length; i++) {
					stage.appendChild(items[i]);
				}
				outer.appendChild(stage);

				args.container.appendChild(outer);
				args.container.appendChild(this.createNavs(args));

				this.position = 1;
				this.itemCount = items.length;
				this.step = 100 / this.itemCount;
				this.outer = outer;
				this.items = items;
				this.stage = stage;
				this.itemCountPerSlide = args.itemCountPerSlide;

				return 'true';
			} catch (e) {
				console.log('Error with plugin');
				console.info(e);
			}
		}
	}, {
		key: 'createNavs',
		value: function createNavs(args) {
			var navs = document.createElement('div');
			navs.classList.add('miracle-slider__nav');
			var prev = document.createElement('div');
			prev.classList.add('miracle-slider__prev');
			prev.innerHTML = '';
			var next = document.createElement('div');
			next.classList.add('miracle-slider__next');
			next.innerHTML = '';

			navs.appendChild(prev);
			navs.appendChild(next);

			return navs;
		}
	}, {
		key: 'setBehavior',
		value: function setBehavior(args) {
			var allmargins = args.margin * (args.itemCountPerSlide - 1);
			var itemWidth = (this.outer.offsetWidth - allmargins) / args.itemCountPerSlide;
			var outer_width = 0;
			this.checkActiveItems(this.position, this.itemCountPerSlide);
			for (var i = 0; i < this.items.length; i++) {
				this.items[i].style.width = itemWidth + 'px';
				outer_width += itemWidth;
				// if ( this.items[i].classList.contains('active') )
				// 	this.items[i].style.marginRight = args.margin + 'px';
				// if ( this.items[i].classList.contains( 'last' ) )
				// 	this.items[i].style.marginRight = 0 + "px";
			}
		}
	}, {
		key: 'listenNav',
		value: function listenNav(args) {
			var _this = this;

			args.container.addEventListener('click', function (e) {
				if (e.target.classList.contains('miracle-slider__next')) {
					_this.moveSlide('next');
				} else if (e.target.classList.contains('miracle-slider__prev')) {
					_this.moveSlide('prev');
				}
			});
		}
	}, {
		key: 'moveSlide',
		value: function moveSlide(direction) {
			if (direction == 'next') {
				this.position++;
				if (this.position == this.items.length - this.itemCountPerSlide + 2) {
					this.position = 1;
				}
			} else if (direction == 'prev') {
				this.position--;
				if (this.position < 1) {
					this.position = this.items.length - this.itemCountPerSlide + 1;
				}
			}
			var position = this.position;
			position--;
			this.stage.style.transform = 'translateX(-' + this.step * position + '%)';
			this.checkActiveItems(this.position, this.itemCountPerSlide);
		}
	}, {
		key: 'checkActiveItems',
		value: function checkActiveItems(position, itemCountPerSlide) {
			for (var i = 0; i < this.items.length; i++) {
				this.items[i].classList.remove('active');
				this.items[i].classList.remove('last');
			}
			for (var i = position; i < position + itemCountPerSlide; i++) {
				this.items[i - 1].classList.add('active');
				if (i == position + itemCountPerSlide - 1) this.items[i - 1].classList.add('last');
			}
		}
	}]);

	return MiracleSlider;
}();

exports.MiracleSlider = MiracleSlider;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PerfectPosition = function () {
	function PerfectPosition(blocks) {
		_classCallCheck(this, PerfectPosition);

		this.setStyle(blocks);
	}

	_createClass(PerfectPosition, [{
		key: 'setStyle',
		value: function setStyle(blocks) {
			for (var i = 0; i < blocks.length; i++) {
				try {
					var block = blocks[i].blockName;
					var offset = blocks[i].offset;
					// let marg = block.offsetHeight + offset;
					var marg = block.offsetHeight;
					block.style.marginTop = '-' + marg + 'px';
					// block.style.marginBottom = offset + 'rem';
				} catch (e) {
					console.info(e);
				}
			}
		}
	}]);

	return PerfectPosition;
}();

exports.PerfectPosition = PerfectPosition;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(0);


/***/ })
/******/ ]);
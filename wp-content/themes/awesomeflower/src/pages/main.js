// var normalize = require('normalize-css');
// var fonts = require('../globals/animate.scss');

//Blocks
import '../global/fonts.scss';
import '../blocks/aboutus/aboutus.block.js';
import '../blocks/constr-decor/constr-decor.block.js';
import '../blocks/contacts/contacts.block.js';
import '../blocks/container/container.block.js';
import '../blocks/footer/footer.block.js';
import '../blocks/modalform/modalform.block.js';
import '../blocks/portfolio/portfolio.block.js';
import '../blocks/shop/shop.block.js';
import '../blocks/slider/slider.block.js';
import '../blocks/team/team.block.js';
import '../blocks/test-header/test-header.block.js';
import '../blocks/wrapper/wrapper.block.js';


// all app.js
import {MiracleSlider} from '../global/miracle-slider/miracle.slider.class';
import {PerfectPosition} from '../global/perfect-position/perfect.position.class';

try{
	document.addEventListener('DOMContentLoaded', function(){
		
		// main slider
		let container = document.querySelector('.miracle-slider-main');
		let args = {
			itemCountPerSlide : {
				320  : 1,
				780  : 1,
				1200 : 1
			},
			autoPlay : true,
			container : container,
			margin: 0
		};
		let slider = new MiracleSlider( args );
		// main slider

		// aboutus_slider
		container = document.querySelector('.aboutus__slider');
		args = {
			itemCountPerSlide : {
				320  : 2,
				780  : 3,
				1200 : 3
			},
			autoPlay : true,
			container : container,
			margin: 10
		};
		let about_slider = new MiracleSlider( args );
		// aboutus_slider

		// portfolio slider
		container = document.querySelector('.portfolio__slider');
		args = {
			itemCountPerSlide : {
				320  : 2,
				780  : 3,
				1200 : 4
			},
			autoPlay : true,
			container : container,
			margin : 20,
			rows : 2
		};
		let portfolio__slider = new MiracleSlider( args );
		// portfolio slider

		// team slider
		container = document.querySelector('.team__slider');
		args = {
			itemCountPerSlide : {
				320  : 1,
				780  : 3,
				1200 : 3
			},
			container : container,
			margin : 15,
		};
		let team_slider = new MiracleSlider( args );
		// team slider

		// correct position
		let correct_blocks = [];
		let block = {
			blockName : document.querySelector('.test-header__laye'), 
			offset : 0
		}
		correct_blocks.push( block );
		
		let perfect = new PerfectPosition( correct_blocks );
		// 

		

		let toggle = document.querySelector( '.test-header__mobile-menu-toggle' );
		// console.log(toggle);
		toggle.addEventListener( 'click', (e)=>{
			document.querySelector( '.test-header__content' ).classList.toggle('test-header__content_toggled');
		});

		let close = document.querySelector( '.test-header__mobile-menu-close' );
		close.addEventListener( 'click', (e)=>{
			document.querySelector( '.test-header__content' ).classList.toggle('test-header__content_toggled');
		});

		let modal = document.querySelector('.modalwindow');
		let btn = document.querySelector("#test-header__myBtn");
		let span = document.querySelector(".modalwindow__close");
		let btn2 = document.querySelector('#zayavka');

		btn.onclick = function(e) {
			e.preventDefault();
			modal.classList.add('modalwindow_opened');
		}

		btn2.onclick = function(e) {
			e.preventDefault();
			modal.classList.add('modalwindow_opened');
		}

		span.onclick = function() {
			modal.classList.add('modalwindow_opened');
		}

		window.onclick = function(event) {
			if (event.target == span) {
				modal.classList.remove('modalwindow_opened');
			}
		}

		var mySwiper = new Swiper ('.swiper-header', {
			// Optional parameters
			direction: 'horizontal',
			loop: true,
			grabCursor: true,

			// If we need pagination
			pagination: {
			  el: '.swiper-pagination',
			},

			// Navigation arrows
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},

			// And if we need scrollbar
			scrollbar: {
			  el: '.swiper-scrollbar',
			},
		})

		var mySwiper = new Swiper ('.swiper-about', {
			// Optional parameters
			direction: 'horizontal',
			loop: true,
			grabCursor: true,
			slidesPerView: 3,
			spaceBetween: 30,

			// Navigation arrows
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				// when window width is <= 320px
				320: {
				  slidesPerView: 1,
				  spaceBetween: 10
				},
				// when window width is <= 480px
				480: {
				  slidesPerView: 2,
				  spaceBetween: 20
				},
				// when window width is <= 640px
				640: {
				  slidesPerView: 3,
				  spaceBetween: 30
				}
			}
		})

		var mySwiper = new Swiper ('.swiper-portfolio', {
			// Optional parameters
			direction: 'horizontal',
			loop: true,
			grabCursor: true,
			slidesPerView: 4,
			slidesPerColumn: 1,
			spaceBetween: 10,

			// Navigation arrows
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				// when window width is <= 320px
				320: {
				  slidesPerView: 1,
				  spaceBetween: 10
				},
				// when window width is <= 480px
				480: {
				  slidesPerView: 2,
				  spaceBetween: 20
				},
				// when window width is <= 640px
				640: {
				  slidesPerView: 3,
				  spaceBetween: 30
				}
			}
		})

		var mySwiper = new Swiper ('.swiper-team', {
			// Optional parameters
			direction: 'horizontal',
			loop: true,
			grabCursor: true,
			slidesPerView: 3,
			spaceBetween: 10,

			// Navigation arrows
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				// when window width is <= 320px
				320: {
				  slidesPerView: 1,
				  spaceBetween: 10
				},
				// when window width is <= 480px
				480: {
				  slidesPerView: 2,
				  spaceBetween: 20
				},
				// when window width is <= 640px
				640: {
				  slidesPerView: 3,
				  spaceBetween: 30
				}
			}
		})

	});

	// document.addEventListener( 'DOMContentLoaded', ()=>{
	// 	var fonts = require('../global/fonts.scss');
	// });

	sendMail();

}

catch(e){
	console.error(e);
}


function sendMail() {
	let forms = document.querySelectorAll("form");

	for (var i = 0; i < forms.length; i++) {
		let form = forms[i];

		form.addEventListener('submit', (e)=> {
			e.preventDefault();

			let image = "";
			let email = form.querySelector("input[type='email']");
			let name = form.querySelector("input[name='firstname']");
			let phone = form.querySelector("input[name='phone']");
			let check = form.querySelector("input[type='checkbox']");
			let subject = form.querySelector("textarea[name='subject']");

			if (check.checked == true) {
				image = document.querySelector("#save").href;
			}

			let	body = "email=" + encodeURIComponent(email.value) + "&"
					+ "name=" + encodeURIComponent(name.value) + "&"
					+ "phone=" + encodeURIComponent(phone.value) + "&"
					+ "subject=" + encodeURIComponent(subject.value) + "&"
					+ "image=" + encodeURIComponent(image) + "&"
					+ "nonce=" + ajax.nonce + "&"
					+ "action=send_mail";
			fetch(ajax.url, 
				{
					method : 'POST',
					headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'},
					body : body
				})
			.then(function(response){
				return response.json();
			})
			.then(function(answer) {
				if (answer == "true") {
					alert("Заявка отправлена. Ожидайте звонка");
				}
			})
			.catch(alert);
		})
	}
}
// all app.js


// document.addEventListener( 'DOMContentLoaded', ()=>{
//     var fonts = require('../globals/font/font.scss');
// });


// import { WOW } from 'wowjs';
// var wow = new WOW(
//     {
//         boxClass:     'miracle-wow',      // animated element css class (default is wow)
//         animateClass: 'animated', // animation css class (default is animated)
//         offset:       0,          // distance to the element when triggering the animation (default is 0)
//         mobile:       true,       // trigger animations on mobile devices (default is true)
//         live:         false,       // act on asynchronously loaded content (default is true)
//         callback:     function(box) {
//           // the callback is fired every time an animation is started
//           // the argument that is passed in is the DOM node being animated
//         },
//         scrollContainer: null // optional scroll container selector, otherwise use window
//     }
// );
// wow.init();

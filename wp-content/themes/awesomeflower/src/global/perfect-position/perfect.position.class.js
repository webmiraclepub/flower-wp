class PerfectPosition{
	constructor( blocks ){
		this.setStyle( blocks );
	}

	setStyle( blocks ){
		for (let i = 0; i < blocks.length; i++) {
			try	{
				let block = blocks[i].blockName;
				let offset = blocks[i].offset;
				// let marg = block.offsetHeight + offset;
				let marg = block.offsetHeight;	
				block.style.marginTop = '-' + marg + 'px';
				// block.style.marginBottom = offset + 'rem';
			}
			catch( e ){
				console.info(e);
			}
		}
	}
}


export {PerfectPosition};
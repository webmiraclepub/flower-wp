class MiracleSlider{
	constructor( args ){
		this.defaultArgs( args );
		if ( this.createWrap( args ) ){
			this.setBehavior( args );
			this.listenNav( args );
		}
	}

	defaultArgs( args ){
		try{
			if ( args.itemCountPerSlide == undefined )
				args.itemCountPerSlide = 3;
			if ( screen.width <= 320 )
				args.itemCountPerSlide = args.itemCountPerSlide[320];
			else if( screen.width <= 780 )
				args.itemCountPerSlide = args.itemCountPerSlide[780];
			else if ( screen.width <= 1200 )
				args.itemCountPerSlide = args.itemCountPerSlide[1200];
			else 
				args.itemCountPerSlide = args.itemCountPerSlide[1200];

			if ( args.autoPlay == undefined )
				args.autoPlay = false;
			if ( args.container == undefined )
				console.info('no container chosen');
			if ( args.margin == undefined )
				args.margin = 10;
			if ( args.rows == undefined )
				args.rows = 1;
		}
		catch(e){
			console.log('Error in plugin');
			console.info(e);
		}
	}
	
	createWrap( args ){
		try{
			let items = args.container.querySelectorAll('.miracle-slider__item');

			let outer = document.createElement('div');
			outer.classList.add('miracle-slider__outer');
			
			let stage = document.createElement('div');
			stage.classList.add('miracle-slider__stage');

			for (let i = 0; i < items.length; i++) {
				stage.appendChild(items[i]);
			}
			outer.appendChild(stage);

			args.container.appendChild(outer);
			args.container.appendChild( this.createNavs( args ) );

			this.position = 1;
			this.itemCount = items.length;
			this.step = 100 / this.itemCount;
			this.outer = outer;
			this.items = items;
			this.stage = stage;
			this.itemCountPerSlide = args.itemCountPerSlide;

			return 'true';
		}
		catch(e){
			console.log('Error with plugin');
			console.info(e);
		}
	}

	createNavs( args ){
		let navs = document.createElement('div');
		navs.classList.add('miracle-slider__nav');
		let prev = document.createElement('div');
		prev.classList.add('miracle-slider__prev');
		prev.innerHTML = '';
		let next = document.createElement('div');
		next.classList.add('miracle-slider__next');
		next.innerHTML = '';

		navs.appendChild(prev);
		navs.appendChild(next);

		return navs;	
	}

	setBehavior( args ){
		let allmargins = args.margin * ( args.itemCountPerSlide - 1 );
		let itemWidth = ( this.outer.offsetWidth - allmargins ) / args.itemCountPerSlide ;
		// костыль. есть проблема с расчетом ширины родительского контейнера для слайдера
		if (args.itemCountPerSlide == 1){
			// itemWidth = window.innerWidth;
			// document.querySelector('.miracle-slider-main .miracle-slider__nav').style.width = window.innerWidth - 30 + "px";
		}
		let outer_width = 0;
		this.checkActiveItems( this.position, this.itemCountPerSlide );
		for (var i = 0; i < this.items.length; i++) {
			this.items[i].style.width = itemWidth + 'px';
			outer_width += itemWidth;
			// if ( this.items[i].classList.contains('active') )
			// 	this.items[i].style.marginRight = args.margin + 'px';
			// if ( this.items[i].classList.contains( 'last' ) )
			// 	this.items[i].style.marginRight = 0 + "px";
		}
	}

	listenNav( args ){
		args.container.addEventListener( 'click', (e)=>{
			if ( e.target.classList.contains('miracle-slider__next') ){
				this.moveSlide( 'next' );
			} else if ( e.target.classList.contains('miracle-slider__prev') ){
				this.moveSlide( 'prev' );
			}
		});
	}

	moveSlide( direction ){
		if ( direction == 'next' ){
			this.position++;
			if ( this.position == ( this.items.length - this.itemCountPerSlide + 2 ) ){
				this.position = 1;
			}
		} else if( direction == 'prev' ) {
			this.position--;
			if ( this.position < 1 ){
				this.position = this.items.length - this.itemCountPerSlide + 1;
			}
		}
		let position = this.position;
		position--;
		this.stage.style.transform = 'translateX(-' + this.step * position + '%)';
		this.checkActiveItems( this.position, this.itemCountPerSlide );
	}

	checkActiveItems( position, itemCountPerSlide ){
		for (var i = 0; i < this.items.length; i++) {
			this.items[i].classList.remove( 'active' );
			this.items[i].classList.remove( 'last' );
		}
		for (var i = position; i < position + itemCountPerSlide; i++) {
			this.items[i - 1].classList.add( 'active' );
			if ( i == position + itemCountPerSlide - 1 )
				this.items[i - 1].classList.add( 'last' );
		}
	}
}

export {MiracleSlider};
<?php

include('lib/init.php');
// include('lib/ajax.php');


register_nav_menu( 'main', 'Главное меню' );


/* PTE and Yoast SEO conflict fix. https://wordpress.org/support/topic/js-error-and-upload-failed */
remove_action('dbx_post_advanced', 'pte_edit_form_hook_redirect');

// Option page(s)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Дополнительные настройки',
		'menu_title'	=> 'Доп настройки',
		'menu_slug'	=> 'acf-options-general',
		'parent_slug'	=> 'options-general.php',
	));
}

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

function true_register_wp_sidebars() {
 
	/* Шапка */
	register_sidebar(
		array(
			'id' => 'header', // уникальный id
			'name' => 'Шапка', // название сайдбара
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.', // описание
			'before_widget' => '<div id="%1$s" class="side widget %2$s">', // по умолчанию виджеты выводятся <li>-списком
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">', // по умолчанию заголовки виджетов в <h2>
			'after_title' => '</h3>'
		)
	);
}
 
add_action( 'widgets_init', 'true_register_wp_sidebars' );
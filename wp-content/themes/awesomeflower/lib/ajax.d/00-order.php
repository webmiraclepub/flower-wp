<?php
	
///////////////
// Init ajax //
///////////////


add_action('wp_ajax_send_message', 'send_message');
add_action('wp_ajax_nopriv_send_message', 'send_message');

function send_message() {
	wp_reset_postdata();
	$nonce = $_POST['nonce'];

	if( ! wp_verify_nonce( $nonce, 'flower' ) )
		wp_die('Ошибка доступа');

	$to  = 'youremail@mail.com';

	$thm = 'Заказ Floral Jungle';

	$html = '
	<html>
	  <head>
		  <title>Заказ Floral Jungle</title>
	  </head>
	  <body>
		  <ul>';
			
	if( !empty( $_POST['firstname'] ) )
		$html .= '<li>Имя: ' . $_POST['firstname'] . '</li>';
	if( !empty( $_POST['phone'] ) )
		$html .= '<li>Телефон: ' . $_POST['phone'] . '</li>';
	if( !empty( $_POST['email'] ) )
		$html .= '<li>Почта: ' . $_POST['email'] . '</li>';
	if( !empty( $_POST['subject'] ) )
		$html .= '<li>Комментарий: ' . $_POST['subject'] . '</li>';
	if( $_POST['checkbox'] == true ){
		$html .= '<li>Цветовые параметры макета</li>';
		$html .= '<li>Лепестки большого цветка: ' . $_POST['big_flower_list'] . '</li>';
		$html .= '<li>Лепестки среднего цветка: ' . $_POST['middle_flower_list'] . '</li>';
		$html .= '<li>Лепестки маленького цветка: ' . $_POST['small_flower_list'] . '</li>';
		$html .= '<li>Тычинки: ' . $_POST['tuchinki'] . '</li>';
		$html .= '<li>Листики: ' . $_POST['lisitiki'] . '</li>';
		$html .= '<li>Стена: ' . $_POST['stena'] . '</li>';
	}

	$html .= '</ul>
	  </body>
	</html>';

	if (!empty($_FILES['draft_plan']['tmp_name'])) {  
		$path = $_FILES['draft_plan']['tmp_name'];
		$filename = $_FILES['draft_plan']['name'];
		$fp = fopen($path,"r"); 
		if (!$fp) { 
		  //print "Файл $path не может быть прочитан"; 
		  //exit(); 
		}
		$file = fread($fp, filesize($path)); 
		fclose($fp); 
		$boundary = "--".md5(uniqid(time())); // генерируем разделитель 
		$headers  = "MIME-Version: 1.0\n";
		$headers .= "From: <zakaz@flower.wp>\r\n";
		$headers .= "Bcc: zakaz@flower.wp\r\n";
		$headers .="Content-Type: multipart/mixed; boundary=\"".$boundary."\"\n";
		$multipart .= "--".$boundary."\n"; 
		$multipart .= "Content-Type: text/html; charset=UTF-8\n"; 
		$multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n"; 
		$multipart .= $html."\n\n"; 
		$message_part = "--".$boundary."\n"; 
		$message_part .= "Content-Type: application/octet-stream\n"; 
		$message_part .= "Content-Transfer-Encoding: base64\n"; 
		$message_part .= "Content-Disposition: attachment; filename = \"".$filename."\"\n\n"; 
		$message_part .= chunk_split(base64_encode($file))."\n"; 
		$multipart .= $message_part."--".$boundary."--\n"; 
		if(mail($to, $thm, $multipart, $headers)) {
			echo json_encode( array( 'answer' => true) );
			//echo "К сожалению, письмо не отправлено"; 
			//exit();
		}else{
			echo json_encode( array( 'answer' => false ) );
		}
	}else{
		$headers  = "MIME-Version: 1.0\n";
		$headers .= "From: <zakaz@flower.wp>\r\n";
		$headers .= "Bcc: zakaz@flower.wp\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\n";
		if( mail($to, $thm, $html, $headers) ){
			echo json_encode( array( 'answer' => true ) );
			// echo $_POST['checkbox'];
		}else{
			echo json_encode( array( 'answer' => false ) );
		}
	}

	wp_die();
}

<?php

//init vars
$styles_path = get_stylesheet_directory_uri() . '/assets/css';
$js_path = get_stylesheet_directory_uri() . '/assets/js';
$dist_path = get_stylesheet_directory_uri() . '/dist';


// init Styles
add_action('wp_enqueue_scripts', 'sicom_init_css');
function sicom_init_css() {
	global $styles_path;
	wp_register_style('app-css', $styles_path.'/app.css', null, null, 'all');
	wp_register_style('style-css', get_stylesheet_directory_uri().'/style.css', null, null, 'all');
	wp_register_style('swiper-css', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.min.css', null, null, 'all');

	wp_enqueue_style( 'style-css' );
	wp_enqueue_style( 'swiper-css' );
}

// init Javascript
add_action('wp_enqueue_scripts', 'sicom_init_js');
function sicom_init_js() {
	global $js_path;
	global $dist_path;
	wp_deregister_script( 'jquery' );
	wp_register_script( 'app-js', $dist_path.'/index.miracle.js', false, null, true);
	wp_register_script( 'temp-js', $js_path.'/temp.js', false, null, true);
	wp_register_script( 'swiper-js', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/js/swiper.min.js', false, null, true);


	wp_enqueue_script( 'app-js' );  
	wp_enqueue_script( 'temp-js' );  
	wp_enqueue_script( 'swiper-js' );

	wp_localize_script('temp-js', 'ajax', 
		array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('flower')
		)
	);

}


/*
 * -- Admin styles
 */ 
add_action( 'admin_enqueue_scripts', 'sicom_admin_styles' );
function sicom_admin_styles() {
	global $styles_path;
	wp_register_style( 'admin_css', $styles_path.'/admin.css', false, '1.0.0' );
	
	wp_enqueue_style( 'admin_css' );
}


foreach(glob(get_stylesheet_directory() . '/lib/func.d/*.php') as $file) {
	include_once $file;
}

foreach(glob(get_stylesheet_directory() . '/lib/ajax.d/*.php') as $file) {
	include_once $file;
}

add_action('wp_ajax_send_mail', 'send_mail');
add_action('wp_ajax_nopriv_send_mail', 'send_mail');

function send_mail() {
	$to  = "ask@floraljungle.com";
	$subject = "Заявка с сайта по цветам";
	$body = "Меня зовут: " . $_POST['name'] . "</br>" .
	"Мой имейл: " . $_POST['email'] . "</br>".
	"Мой телефон: " . $_POST['phone'] . "</br>".
	"Я хочу вот такое: " . $_POST['subject'] . "</br>".
	"Картинка, которую я выбрал: <a href='" . $_POST['image'] . "'>вот тут</a></br>";
	$message = '
		<html>
		    <head>
		        <title>' . $subject . '</title>
		    </head>
		    <body>'
			. $body
			.'</body>
		</html>';

	$headers = 'Content-type: text/html; charset=iso-8859-1';

	$mail = mail($to, $subject, $message, $headers);

	$answer = array();
	array_push($answer, $mail);
	echo json_encode( $answer );
	wp_die();
}